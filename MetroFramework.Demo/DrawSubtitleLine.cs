﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace weixinTucaoClient
{
    public class DrawSubtitleLine
    {
        public int GapBetweenSubtitles = 50;//两条吐槽之间的间隔
        public bool IsEmpty = true;
        public List<Subtitle> subtitleList = new List<Subtitle>();
        public float y = 0;
        public DrawSubtitleLine(float _y)
        {
            this.y = _y;
        }
        public void addNewSubtitle(Subtitle _subtitle)
        {
            if (_subtitle == null)
            {
                return;
            }
            else
            {
                this.IsEmpty = false;
                _subtitle.SetY(this.y);
                this.subtitleList.Add(_subtitle);
            }
        }

        public void DrawSubtitle(Graphics g)
        {
            this.subtitleList.ForEach((_subtitle) =>
            {
                _subtitle.DrawSubtitle(g);

            });

            this.subtitleList = this.subtitleList.FindAll((_subtitle) =>
            {
                return _subtitle.DrawingState == true;
            });
            var count = this.subtitleList.Count;
            if (count > 0)
            {
                var lastOne = this.subtitleList[count - 1];
                if (lastOne.TailHasEnoughSpace(this.GapBetweenSubtitles))
                {
                    this.IsEmpty = true;
                }
            }
        }
    }
}
