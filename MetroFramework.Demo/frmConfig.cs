﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WebSocketSharp;

namespace weixinTucaoClient
{
    public partial class frmConfig : MetroForm
    {
        string subscriberID = "";
        string wssUrl = "ws://111.67.197.251?user=";
        WebSocket ws = null;
        frmSubtitle frmSubtitle = null;
        MouseHook mouse = new MouseHook();
        bool playing = true;
        Timer _timer = new Timer();
        public frmConfig()
        {
            InitializeComponent();

            Screen[] screens = System.Windows.Forms.Screen.AllScreens;
            for (int i = 0; i < screens.Length; i++)
            {
                Screen sc = screens[i];
                if (sc.Primary == true)
                {
                    Rectangle rect = sc.WorkingArea;
                    this.Width = rect.Width;
                    this.Left = 0;
                    this.Top = -(this.Height - 10);
                }
            }
            var gap = 20;
            var totalWidth = this.btnControl.Width + this.btnBlack.Width + this.btnRed.Width + this.btnWhite.Width + this.btnYellow.Width + 4 * gap;
            var left = (this.Width - totalWidth) / 2;
            this.btnControl.Left = left;
            this.btnBlack.Left = left + btnControl.Width + gap;
            this.btnWhite.Left = this.btnBlack.Left + btnBlack.Width + gap;
            this.btnRed.Left = this.btnWhite.Left + btnWhite.Width + gap;
            this.btnYellow.Left = this.btnRed.Left + btnRed.Width + gap;

            this.btnBlack.Click += (a1, a2) => { setSubtitleColor(btnBlack.BackColor); };
            this.btnWhite.Click += (a1, a2) => { setSubtitleColor(btnWhite.BackColor); };
            this.btnRed.Click += (a1, a2) => { setSubtitleColor(btnRed.BackColor); };
            this.btnYellow.Click += (a1, a2) => { setSubtitleColor(btnYellow.BackColor); };

            this.Shown += (a1, a2) =>
            {
                frmSubtitle = new frmSubtitle();
                frmSubtitle.Show();
                this.programInitial();
                wssUrl = "ws://111.67.197.251?user=" + subscriberID;

                setupWebsocket(wssUrl);

                _timer.Interval = 10 * 1000;
                _timer.Tick += (a3, a4) =>
                {
                    if (ws != null)
                    {
                        ws.Send("live");
                    }
                };
                _timer.Start();
            };

            this.btnControl.Click += (a1, a2) =>
            {
                //frmSubtitle.doSomeTestData();
                if (this.playing == true)
                {
                    this.playing = false;
                    this.btnControl.Image = global::weixinTucaoClient.Properties.Resources.play;
                    if (this.frmSubtitle != null) frmSubtitle.Pause();
                }
                else
                {
                    this.playing = true;
                    this.btnControl.Image = global::weixinTucaoClient.Properties.Resources.pause;
                    if (this.frmSubtitle != null) frmSubtitle.Play();
                }
            };

            this.FormClosing += (a1, a2) =>
            {
                if (this.frmSubtitle != null)
                {
                    this.frmSubtitle.Close();
                }
                mouse.Stop();
            };


            mouse.OnMouseActivity += (mouse_OnMouseActivity);
            mouse.Start();
        }


        void setupWebsocket(string url)
        {
            try
            {
                Console.WriteLine("create websocket conection on " + url);
                ws = new WebSocket(url);

                ws.OnOpen += (sender, e) =>
                {
                    Console.WriteLine(" websocket conection opened ");

                };

                ws.OnMessage += (sender, e) =>
                {
                    Console.WriteLine(e.Data);
                    List<TucaoMessage> list = (List<TucaoMessage>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<TucaoMessage>>(e.Data);
                    addNewTucao(list);
                };
                ws.OnError += (sender, e) =>
                {
                    Console.WriteLine(e.Message);
                    //setupWebsocket(wssUrl);
                    MessageBox.Show("建立网络连接失败，系统无法正常工作！");
                    Application.Exit();
                };

                ws.OnClose += (sender, e) =>
                {
                    Console.WriteLine(e.Data);
                };

                ws.Connect();
            }
            catch (Exception e)
            {
                Console.WriteLine("websocket 异常：" + e.Message);
            }
        }

        void addNewTucao(List<TucaoMessage> list)
        {
            list.ForEach((_tucao) =>
            {
                if (this.frmSubtitle != null)
                {
                    var data = string.Format("{0}: {1}", _tucao.name, _tucao.Content);
                    frmSubtitle.addTitleData(data);
                }
            });
        }

        void setSubtitleColor(Color _color)
        {
            if (this.frmSubtitle != null)
                this.frmSubtitle.resetSubtitleColor(_color);
        }
        void mouse_OnMouseActivity(object sender, MouseEventArgs e)
        {
            string str = "X:" + e.X + "  Y:" + e.Y;
            //Console.WriteLine(str);
            if (e.Y < 10 && this.Top != 0)
            {
                this.Top = 0;

            }
            if (e.Y > (this.Height + 10) && this.Top == 0)
            {
                this.Top = -(this.Height - 10);
            }

        }
        private void programInitial()
        {
            object o = nsConfigDB.ConfigDB.getConfig("subscriberID");
            if (o != null)
            {
                subscriberID = (string)o;
            }
            //else {
            //    nsConfigDB.ConfigDB.saveConfig("subscriberID", "111");
            //}

        }

    }
}
