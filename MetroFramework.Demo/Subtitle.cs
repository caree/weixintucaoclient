﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace weixinTucaoClient
{
    public class Subtitle
    {
        public bool DrawingState = true;//默认没有绘制完成一个流程
        public string TitleContent = string.Empty;
        public float titleWidth = -1;
        public float x = 0;//绘制的x坐标
        public float y = 0;//绘制的y坐标
        public int xStep = 1;//每次横向移动的距离
        public float xStart = 0;
        Font font;
        Brush brush = null;
        //Graphics g;
        public Subtitle(Brush brush, Font font, string content, float xStart, int _xStep)
        {
            //this.g = g;
            this.brush = brush;
            this.font = font;
            this.TitleContent = content;
            this.xStart = xStart;
            this.x = xStart;
            this.xStep = _xStep;
        }
        public void SetBrush(Brush _brush)
        {
            this.brush = _brush;
        }
        public void SetY(float y)
        {
            this.y = y;
        }
        public bool TailHasEnoughSpace(int gap)
        {
            return (int)(this.xStart) > (this.x + this.titleWidth + gap);
        }
        public void DrawSubtitle(Graphics g)
        {
            if (this.DrawingState)
            {
                if (this.titleWidth < 0)
                {
                    SizeF size = g.MeasureString(this.TitleContent, font);
                    this.titleWidth = size.Width;
                }
                g.DrawString(this.TitleContent, font, brush, x, y);
                //绘制宋体字体

                g.DrawString(this.TitleContent, font, brush, x, y);
                //Console.WriteLine(string.Format("x = {0}  size = {1}", x, size.Width));
                if ((-x) > this.titleWidth)
                {
                    Console.WriteLine(this.TitleContent + " 滚动结束");
                    this.DrawingState = false;
                }
                x -= xStep;
            }

        }
    }
}
