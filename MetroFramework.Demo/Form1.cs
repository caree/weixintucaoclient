﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace weixinTucaoClient
{
    public partial class Form1 : MetroForm
    {
        Graphics g = null;
        Timer timer = new Timer();
        Brush brush = null;
        Font font = null;
        //Color subtitleColor = Color.Black;
        int xStep = 3;//每次横向移动的距离
        float xStart = 0;
        float yStart = 10;
        int yDistance = 50;//纵向两行之间的距离
        //List<string> strList = new List<string>();
        //int fontSize = 20;
        int maxLine = 3;//最大字幕行数
        int timerRefreshInterval = 30;

        int g_temp_width = 0;
        int g_temp_height = 0;

        List<Subtitle> subtitleList = new List<Subtitle>();
        List<DrawSubtitleLine> drawingLineList = new List<DrawSubtitleLine>();
        public Form1()
        {
            InitializeComponent();
            this.pictureBox1.Width = this.Width;

            this.g_temp_height = this.pictureBox1.Height;
            this.g_temp_width = this.pictureBox1.Width;

            brush = new SolidBrush(Color.Black);
            g = this.pictureBox1.CreateGraphics();
            xStart = this.pictureBox1.Width;
            resetConfig(20, this.yDistance, this.xStep, this.timerRefreshInterval);

            this.metroButton1.Click += metroButton1_Click;
            this.metroButton2.Click += metroButton2_Click;

            this.metroButton3.Click += startloopText;
            timer.Tick += (a1, a2) =>
            {
                //g.Clear(Color.White);

                Graphics g_temp;
                Bitmap drawing = null;
                drawing = new Bitmap(g_temp_width, g_temp_height, g);
                g_temp = Graphics.FromImage(drawing);
                g_temp.Clear(Color.White);

                Action<DrawSubtitleLine> tryToGetSubtitle = (_line) =>
                {
                    if (subtitleList.Count > 0)
                    {
                        var title = subtitleList[0];
                        subtitleList.Remove(title);
                        _line.addNewSubtitle(title);
                    }
                };
                drawingLineList.ForEach((_line) =>
                {
                    _line.DrawSubtitle(g_temp);
                    if (_line.IsEmpty)
                    {
                        //Console.WriteLine("no subtitle drawing, try to get...");
                        tryToGetSubtitle(_line);
                        return;
                    }
                });

                g.DrawImageUnscaled(drawing, 0, 0);
                g_temp.Dispose();
                drawing.Dispose();
            };

            this.Shown += (a1, a2) =>
            {
                this.timer.Start();
            };

            this.btnBlack.Click += (a1, a2) =>
            {
                resetSubtitleColor(btnBlack.BackColor);
            };

            this.btnGreen.Click += (a1, a2) =>
            {
                resetSubtitleColor(btnGreen.BackColor);
            };
            this.btnRed.Click += (a1, a2) =>
            {
                resetSubtitleColor(btnRed.BackColor);
            };


            this.btnAddTestData.Click += (a1, a2) =>
            {
                doSomeTestData();
            };
            drawingLineList.Clear();
            for (var i = 0; i < maxLine; i++)
            {
                drawingLineList.Add(new DrawSubtitleLine(yStart + i * yDistance));
            }
        }
        void resetSubtitleColor(Color _color)
        {
            this.brush = new SolidBrush(_color);
            subtitleList.ForEach((_subtitle) =>
            {
                _subtitle.SetBrush(this.brush);
            });
        }
        void resetConfig(int _fontSize, int _yDistance, int _xStep, int _timerRefreshInterval)
        {
            //timer.Stop();

            //if (font != null)
            //{
            //    font.Dispose();
            //}
            font = new Font("宋体", _fontSize, FontStyle.Bold, GraphicsUnit.Point);
            yDistance = _yDistance;
            xStep = _xStep;
            timer.Interval = _timerRefreshInterval;
            //if (this.brush != null)
            //{
            //    this.brush.Dispose();
            //}
            //this.brush = new SolidBrush(_subtitleColor);


            //timer.Start();
        }

        void startloopText(object sender, EventArgs e)
        {
            //doSomeTestData();

            resetConfig(int.Parse(this.txtFontSize.Text), int.Parse(this.txtYDistance.Text), int.Parse(this.txtDistance.Text), int.Parse(this.txtInterval.Text));
        }
        void doSomeTestData()
        {
            var str = "绘制宋体字体";
            subtitleList.Add(new Subtitle(brush, font, str, xStart, xStep));
            subtitleList.Add(new Subtitle(brush, font, str, xStart, xStep));
            subtitleList.Add(new Subtitle(brush, font, str, xStart, xStep));
        }

        void metroButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            g.Clear(Color.White);
            Brush _brush = new SolidBrush(Color.Red);
            //绘制宋体字体
            string time = DateTime.Now.ToLongTimeString();
            Font _font = new Font("宋体", 25, FontStyle.Bold, GraphicsUnit.Point);
            g.DrawString("绘制宋体字体" + time, _font, _brush, 20, 20);

            //绘制隶书字体
            _font = new Font("隶书", 25, FontStyle.Bold, GraphicsUnit.Point);
            g.DrawString("绘制隶书字体" + time, _font, _brush, 20, 80);

            //绘制幼圆字体
            _font = new Font("幼圆", 25, FontStyle.Bold, GraphicsUnit.Point);
            g.DrawString("绘制幼圆字体" + time, _font, _brush, 20, 140);
            //释放资源
            _font.Dispose();
            _brush.Dispose();
        }
    }
}
