﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace weixinTucaoClient
{
    public partial class frmSubtitle : MetroForm
    {
        Graphics g = null;
        Timer timer = new Timer();
        Brush brush = null;
        Font font = null;
        int xStep = 3;//每次横向移动的距离
        float xStart = 0;
        float yStart = 20;
        int yDistance = 50;//纵向两行之间的距离
        int maxLine = 3;//最大字幕行数
        int timerRefreshInterval = 40;
        bool paused = false;//暂停滚动字幕
        int g_temp_width = 0;
        int g_temp_height = 0;
        List<Subtitle> subtitleList = new List<Subtitle>();
        List<DrawSubtitleLine> drawingLineList = new List<DrawSubtitleLine>();
        public frmSubtitle()
        {
            InitializeComponent();
            //this.pictureBox1.Width = this.Width;
            //this.pictureBox1.Height = this.Height;

            brush = new SolidBrush(Color.WhiteSmoke);
            resetConfig(20, this.yDistance, this.xStep, this.timerRefreshInterval);

            this.metroButton2.Click += metroButton2_Click;

            timer.Tick += (a1, a2) =>
            {

                if (paused == true)
                {
                    g.Clear(Color.White);
                    return;
                }

                Graphics g_temp;
                Bitmap drawing = null;
                drawing = new Bitmap(g_temp_width, g_temp_height, g);
                g_temp = Graphics.FromImage(drawing);
                g_temp.Clear(Color.White);

                Action<DrawSubtitleLine> tryToGetSubtitle = (_line) =>
                {
                    if (subtitleList.Count > 0)
                    {
                        var title = subtitleList[0];
                        subtitleList.Remove(title);
                        _line.addNewSubtitle(title);
                    }
                };
                drawingLineList.ForEach((_line) =>
                {
                    _line.DrawSubtitle(g_temp);
                    if (_line.IsEmpty)
                    {
                        //Console.WriteLine("no subtitle drawing, try to get...");
                        tryToGetSubtitle(_line);
                        return;
                    }
                });

                g.DrawImageUnscaled(drawing, 0, 0);
                g_temp.Dispose();
                drawing.Dispose();
            };

            this.Shown += (a1, a2) =>
            {
                this.pictureBox1.Width = this.Width;
                this.pictureBox1.Height = this.Height;
                this.g_temp_height = this.pictureBox1.Height;
                this.g_temp_width = this.pictureBox1.Width;

                g = this.pictureBox1.CreateGraphics();
                xStart = this.pictureBox1.Width;
                this.timer.Start();
            };

            drawingLineList.Clear();
            for (var i = 0; i < maxLine; i++)
            {
                drawingLineList.Add(new DrawSubtitleLine(yStart + i * yDistance));
            }
        }
        public void Pause()
        {
            this.paused = true;
        }
        public void Play()
        {
            this.paused = false;
        }
        public void resetSubtitleColor(Color _color)
        {
            this.brush = new SolidBrush(_color);
            subtitleList.ForEach((_subtitle) =>
            {
                _subtitle.SetBrush(this.brush);
            });
        }
        void resetConfig(int _fontSize, int _yDistance, int _xStep, int _timerRefreshInterval)
        {
            font = new Font("宋体", _fontSize, FontStyle.Bold, GraphicsUnit.Point);
            yDistance = _yDistance;
            xStep = _xStep;
            timer.Interval = _timerRefreshInterval;
        }
        public void addTitleData(string _data)
        {
            subtitleList.Add(new Subtitle(brush, font, _data, xStart, xStep));
        }
        public void doSomeTestData()
        {
            var str = "绘制宋体字体";
            subtitleList.Add(new Subtitle(brush, font, str, xStart, xStep));
            subtitleList.Add(new Subtitle(brush, font, str, xStart, xStep));
            subtitleList.Add(new Subtitle(brush, font, str, xStart, xStep));
        }

        void metroButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
