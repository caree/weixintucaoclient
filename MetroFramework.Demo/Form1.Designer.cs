﻿namespace weixinTucaoClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            this.metroButton3 = new MetroFramework.Controls.MetroButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.txtInterval = new MetroFramework.Controls.MetroTextBox();
            this.txtDistance = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.txtYDistance = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.txtFontSize = new MetroFramework.Controls.MetroTextBox();
            this.btnRed = new System.Windows.Forms.Button();
            this.btnBlack = new System.Windows.Forms.Button();
            this.btnGreen = new System.Windows.Forms.Button();
            this.btnAddTestData = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(917, 308);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // metroButton1
            // 
            this.metroButton1.Highlight = false;
            this.metroButton1.Location = new System.Drawing.Point(37, 507);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(75, 23);
            this.metroButton1.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroButton1.StyleManager = null;
            this.metroButton1.TabIndex = 1;
            this.metroButton1.Text = "metroButton1";
            this.metroButton1.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // metroButton2
            // 
            this.metroButton2.Highlight = false;
            this.metroButton2.Location = new System.Drawing.Point(566, 507);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(75, 23);
            this.metroButton2.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroButton2.StyleManager = null;
            this.metroButton2.TabIndex = 2;
            this.metroButton2.Text = "quit";
            this.metroButton2.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // metroButton3
            // 
            this.metroButton3.Highlight = false;
            this.metroButton3.Location = new System.Drawing.Point(172, 507);
            this.metroButton3.Name = "metroButton3";
            this.metroButton3.Size = new System.Drawing.Size(75, 23);
            this.metroButton3.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroButton3.StyleManager = null;
            this.metroButton3.TabIndex = 3;
            this.metroButton3.Text = "重置参数";
            this.metroButton3.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.CustomBackground = false;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Medium;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Light;
            this.metroLabel1.LabelMode = MetroFramework.Controls.MetroLabelMode.Default;
            this.metroLabel1.Location = new System.Drawing.Point(167, 367);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(107, 19);
            this.metroLabel1.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroLabel1.StyleManager = null;
            this.metroLabel1.TabIndex = 4;
            this.metroLabel1.Text = "刷新时间间隔：";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroLabel1.UseStyleColors = false;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.CustomBackground = false;
            this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.Medium;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Light;
            this.metroLabel2.LabelMode = MetroFramework.Controls.MetroLabelMode.Default;
            this.metroLabel2.Location = new System.Drawing.Point(167, 402);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(107, 19);
            this.metroLabel2.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroLabel2.StyleManager = null;
            this.metroLabel2.TabIndex = 4;
            this.metroLabel2.Text = "单次移动距离：";
            this.metroLabel2.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroLabel2.UseStyleColors = false;
            // 
            // txtInterval
            // 
            this.txtInterval.FontSize = MetroFramework.MetroTextBoxSize.Small;
            this.txtInterval.FontWeight = MetroFramework.MetroTextBoxWeight.Regular;
            this.txtInterval.Location = new System.Drawing.Point(281, 366);
            this.txtInterval.Multiline = false;
            this.txtInterval.Name = "txtInterval";
            this.txtInterval.SelectedText = "";
            this.txtInterval.Size = new System.Drawing.Size(105, 23);
            this.txtInterval.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtInterval.StyleManager = null;
            this.txtInterval.TabIndex = 5;
            this.txtInterval.Text = "30";
            this.txtInterval.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtInterval.UseStyleColors = false;
            // 
            // txtDistance
            // 
            this.txtDistance.FontSize = MetroFramework.MetroTextBoxSize.Small;
            this.txtDistance.FontWeight = MetroFramework.MetroTextBoxWeight.Regular;
            this.txtDistance.Location = new System.Drawing.Point(281, 401);
            this.txtDistance.Multiline = false;
            this.txtDistance.Name = "txtDistance";
            this.txtDistance.SelectedText = "";
            this.txtDistance.Size = new System.Drawing.Size(105, 23);
            this.txtDistance.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtDistance.StyleManager = null;
            this.txtDistance.TabIndex = 5;
            this.txtDistance.Text = "2";
            this.txtDistance.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtDistance.UseStyleColors = false;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.CustomBackground = false;
            this.metroLabel3.FontSize = MetroFramework.MetroLabelSize.Medium;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Light;
            this.metroLabel3.LabelMode = MetroFramework.Controls.MetroLabelMode.Default;
            this.metroLabel3.Location = new System.Drawing.Point(167, 432);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(79, 19);
            this.metroLabel3.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroLabel3.StyleManager = null;
            this.metroLabel3.TabIndex = 4;
            this.metroLabel3.Text = "纵向行距：";
            this.metroLabel3.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroLabel3.UseStyleColors = false;
            // 
            // txtYDistance
            // 
            this.txtYDistance.FontSize = MetroFramework.MetroTextBoxSize.Small;
            this.txtYDistance.FontWeight = MetroFramework.MetroTextBoxWeight.Regular;
            this.txtYDistance.Location = new System.Drawing.Point(281, 428);
            this.txtYDistance.Multiline = false;
            this.txtYDistance.Name = "txtYDistance";
            this.txtYDistance.SelectedText = "";
            this.txtYDistance.Size = new System.Drawing.Size(105, 23);
            this.txtYDistance.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtYDistance.StyleManager = null;
            this.txtYDistance.TabIndex = 5;
            this.txtYDistance.Text = "50";
            this.txtYDistance.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtYDistance.UseStyleColors = false;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.CustomBackground = false;
            this.metroLabel4.FontSize = MetroFramework.MetroLabelSize.Medium;
            this.metroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Light;
            this.metroLabel4.LabelMode = MetroFramework.Controls.MetroLabelMode.Default;
            this.metroLabel4.Location = new System.Drawing.Point(167, 461);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(79, 19);
            this.metroLabel4.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroLabel4.StyleManager = null;
            this.metroLabel4.TabIndex = 4;
            this.metroLabel4.Text = "字体大小：";
            this.metroLabel4.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroLabel4.UseStyleColors = false;
            // 
            // txtFontSize
            // 
            this.txtFontSize.FontSize = MetroFramework.MetroTextBoxSize.Small;
            this.txtFontSize.FontWeight = MetroFramework.MetroTextBoxWeight.Regular;
            this.txtFontSize.Location = new System.Drawing.Point(281, 457);
            this.txtFontSize.Multiline = false;
            this.txtFontSize.Name = "txtFontSize";
            this.txtFontSize.SelectedText = "";
            this.txtFontSize.Size = new System.Drawing.Size(105, 23);
            this.txtFontSize.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtFontSize.StyleManager = null;
            this.txtFontSize.TabIndex = 5;
            this.txtFontSize.Text = "20";
            this.txtFontSize.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtFontSize.UseStyleColors = false;
            // 
            // btnRed
            // 
            this.btnRed.BackColor = System.Drawing.Color.Red;
            this.btnRed.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnRed.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRed.Location = new System.Drawing.Point(447, 398);
            this.btnRed.Name = "btnRed";
            this.btnRed.Size = new System.Drawing.Size(129, 27);
            this.btnRed.TabIndex = 6;
            this.btnRed.UseVisualStyleBackColor = false;
            // 
            // btnBlack
            // 
            this.btnBlack.BackColor = System.Drawing.Color.Black;
            this.btnBlack.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnBlack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBlack.Location = new System.Drawing.Point(447, 362);
            this.btnBlack.Name = "btnBlack";
            this.btnBlack.Size = new System.Drawing.Size(129, 27);
            this.btnBlack.TabIndex = 6;
            this.btnBlack.UseVisualStyleBackColor = false;
            // 
            // btnGreen
            // 
            this.btnGreen.BackColor = System.Drawing.Color.ForestGreen;
            this.btnGreen.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnGreen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGreen.Location = new System.Drawing.Point(447, 434);
            this.btnGreen.Name = "btnGreen";
            this.btnGreen.Size = new System.Drawing.Size(129, 27);
            this.btnGreen.TabIndex = 6;
            this.btnGreen.UseVisualStyleBackColor = false;
            // 
            // btnAddTestData
            // 
            this.btnAddTestData.Highlight = false;
            this.btnAddTestData.Location = new System.Drawing.Point(292, 507);
            this.btnAddTestData.Name = "btnAddTestData";
            this.btnAddTestData.Size = new System.Drawing.Size(94, 23);
            this.btnAddTestData.Style = MetroFramework.MetroColorStyle.Blue;
            this.btnAddTestData.StyleManager = null;
            this.btnAddTestData.TabIndex = 3;
            this.btnAddTestData.Text = "添加测试数据";
            this.btnAddTestData.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(917, 574);
            this.Controls.Add(this.btnBlack);
            this.Controls.Add(this.btnGreen);
            this.Controls.Add(this.btnRed);
            this.Controls.Add(this.txtFontSize);
            this.Controls.Add(this.txtYDistance);
            this.Controls.Add(this.txtDistance);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.txtInterval);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.btnAddTestData);
            this.Controls.Add(this.metroButton3);
            this.Controls.Add(this.metroButton2);
            this.Controls.Add(this.metroButton1);
            this.Controls.Add(this.pictureBox1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "Form1";
            this.TransparencyKey = System.Drawing.Color.DarkGray;
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroButton metroButton2;
        private MetroFramework.Controls.MetroButton metroButton3;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox txtInterval;
        private MetroFramework.Controls.MetroTextBox txtDistance;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox txtYDistance;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox txtFontSize;
        private System.Windows.Forms.Button btnRed;
        private System.Windows.Forms.Button btnBlack;
        private System.Windows.Forms.Button btnGreen;
        private MetroFramework.Controls.MetroButton btnAddTestData;

    }
}